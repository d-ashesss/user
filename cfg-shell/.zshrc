<% if (this_is_important) { print('THIS_IS_IMPORTANT=1'); } %>

<% if (load_grml) { print('source $HOME/.config/user-cfg/zsh-grml'); } %>
source $HOME/.config/user-cfg/zsh
if [ -f /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
	source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi
