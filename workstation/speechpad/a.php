<?php

class A extends MY_Controller {
	public function index() {
	}

	public function __construct() {
		if (defined('STDERR')) fclose(STDERR);
		error_reporting(error_reporting() & ~E_STRICT);
		error_reporting(-1 & ~E_STRICT & ~E_DEPRECATED);
		set_time_limit(0);
		ini_set('display_errors', 'on');
		ini_set('memory_limit', '128M');
		$_SERVER['SERVER_NAME'] = 'www.speechpad.com';
		header('Content-Type: text/plain');
		parent::__construct();

		if (defined('CMD')) {
			$this->auth_default = 0;
		}

		$this->CI = get_instance();
		$this->slaveDb = $this->db;

		$this->load->library('Color');
		$this->load->library('SP_reporting');

		$this->load->helper('date');
		$this->load->helper('uuid');

		$this->load->model('User_model');
		$this->load->model('Audio_model');
		$this->load->model('Job_model');
		$this->load->model('Task_model');
		$this->load->model('Sub_task_model');
		$this->load->model('Transcription_model');
		$this->load->model('API_task_model');

		include_once APPPATH . 'models/Marketplaces/MechanicalTurk/lib/MTurk.php';

		/*
		@var CI_DB_mysql_result $query
		'XDEBUG_SESSION_START' => 'true',
		$url .= "?XDEBUG_SESSION_START=true";
		*/
	}

	///////////////////
	// UTILITY TASKS
	//

	public function generate_video_meta() {
		require_once REUSEREPO . 'Sp/Utils/Ops/Search.php';
		$args = implode(' ', func_get_args());
		$jobs = \Sp\Utils\Ops\Search::parse($args);

		foreach ($jobs as $jobId) {
			/** @var Job $job */
			$job = $this->Job_model->findById($jobId);
			/** @var Audio $audio */
			$audio = $job->getMedia();
			$meta = $audio->getMetadataJson();
			$this->vault->save_data($meta, 'audio_files', $audio->id(), "original_video-meta", '', 'json');
		}
	}

	public function add_asset() {
		$args = func_get_args();
		$user_id = array_shift($args);
		$file_path = str_replace('\\', '/', implode('/', $args));
		if (!file_exists($file_path)) {
			apr('File not found: ' . $file_path, 0, 'yellow');
			return;
		}
		$ret = $this->call_rest('account', 'add_asset', [
			'method' => 'post',
			'visible_filename' => basename($file_path),
		], $user_id, [
			'post_file' => $file_path,
		]);
		if (empty($ret) || empty($ret['error_string']) || empty($ret['response'])) {
			apr('Failed', 0, 'red');
			return;
		}
		if ($ret['error_string'] !== 'SUCCESS') {
			apr($ret['response'], 0, 'red');
			return;
		}
		apr('UUID: ' . $ret['response']['uuid'], 0, 'cyan');
	}

	/**
	 * @param $user_id
	 * @param string $service_code TRANSCRIPTION-120HR TRANSCRIPTION_REVIEW-120HR STD_CAPTIONS-1WK CAPTION_REVIEW-1WK SDH_CAPTIONS-1WK
	 * @param string $media_asset_id
	 *               dff95858-5641-4ff0-9707-1045d964f839 video 360p
	 * @param string $text_asset_id
	 *               ac31a118-875f-450e-b15f-902c07827e63 srt
	 *               6938a77a-00d1-49d1-9dfb-bd3ee0d4ab0b txt
	 *               a1f617b0-274d-48ef-88a7-7c868b3fba19 timed txt
	 * @param array $params
	 */
	public function order_job($user_id, $service_code, $media_asset_id, $text_asset_id = '', $params = []) {
		$ret = $this->call_rest('account', 'job', array_replace([
			'method' => 'post',
			'no_auto_transcribe' => 1,
			'service_code' => $service_code,
			'media_asset_id' => $media_asset_id,
			'text_asset_id' => $text_asset_id,
		], $params), $user_id);
		if (empty($ret) || empty($ret['error_string']) || empty($ret['response'])) {
			apr('Failed', 0, 'red');
			return;
		}
		if ($ret['error_string'] !== 'SUCCESS') {
			apr($ret['response'], 0, 'red');
			return;
		}
		apr('A' . $ret['response']['audio_id'] . ' J' . $ret['response']['job_id'], 0, 'cyan');
	}

	public function fill_order_source() {
		$this->load->model('Audio_model');
		$this->load->model('Order_model');
		$this->load->model('Order_credits_debit_model');
		$this->load->model('Order_item_model');
		/** @var Order[] $orders */
		$orders = $this->Order_model->find(['source is null'], "70", "id desc");
		foreach ($orders as $order) {
			apr($order->id(), 10);
			/** @var Order_credits_debit[] $ocds */
			$ocds = $this->Order_credits_debit_model->find([
				'order_id' => $order->id(),
				"status != 'REFUND'",
			]);
			$ocd_ids = [];
			foreach ($ocds as $ocd) {
				$ocd_ids[] = $ocd->id();
			}
			if (empty($ocd_ids)) {
				echo "\n";
				continue;
			} elseif (count($ocd_ids) === 1) {
				apr($ocd_ids[0], 10);
			} else {
				apr('(' . count($ocd_ids) . ')', 10);
			}
			/** @var Order_item[] $items */
			$items = $this->Order_item_model->find([
				'order_credit_debit_id' => $ocd_ids,
				'media_object_type' => 'AudioFile',
			], 1);
			if (empty($items)) {
				echo "\n";
				continue;
			}
			apr($items[0]->id(), 10);
			/** @var Audio $audio */
			$audio = $this->Audio_model->findById($items[0]->mediaObjectId());
			if (empty($audio)) {
				echo "\n";
				continue;
			}
			apr($audio->id(), 10);
			apr($audio->uploadMethod(), 10);
			if (in_array($audio->uploadMethod(), [
				Audio_model::$UM_DEFAULT,
				Audio_model::$UM_UPLOAD,
				Audio_model::$UM_DEV_URL,
			])) {
				$order->setSource($audio->uploadMethod());
				$order->persist();
				echo "+";
			} elseif (in_array($audio->uploadMethod(), [Audio_model::$UM_URL])) {
				$order->setSource(Audio_model::$UM_DEFAULT);
				$order->persist();
				echo "++";
			} else {
				$order->setSource('Other');
				$order->persist();
				echo "+++";
			}
			echo "\n";
		}
	}

	private $subtitleOptions = [];
	public function burn_subtitles() {
		require_once REUSEREPO . 'Sp/Utils/Ops/Search.php';
		$args = func_get_args();

		$target = '';
		if (!empty($args[0]) && !is_numeric($args[0])) {
		$target = array_shift($args) ?: '';
		if ($target === '-') {
			$target = '';
		}
		$target = str_replace('@today', date('n-j-y'), $target);
		}

		$args = implode(' ', $args);
		$jobs = \Sp\Utils\Ops\Search::parse($args);

		$max_i = count($jobs) - 1;
		foreach ($jobs as $i => $job_id) {
			/** @var Job $job */
			$job = $this->Job_model->findById($job_id);
			/** @var Audio $audio */
			$audio = $job->getMedia();
			$this->load->library('SP_event_manager');
			$stylePreset = $audio->getJobProperty('BURN_CAPTIONS_STYLE'); // ex BURN_SUBTITLES_STYLE_PRESET
			if (is_null($stylePreset)) {
				if (in_array($job->userId(), [
					131060, // hmenicucci@about.com
					360596, // office@aretechiro.com
					364248, // timkirkrealestate@gmail.com
					314285, // rmcnamar@skidmore.edu
				])) {
					$stylePreset = 'hugs_text'; // ex aboutcom3
				} else {
					$stylePreset = 'black_bar'; // ex aboutcom2
				}
			}
			$params = [
				'job_id' => $job->id(),
				'audio_id' => $audio->id(),
				'target' => $target,
				'style' => $stylePreset,

#				'target' => 'Revisions 6-15-16',
#				'target' => '8-9-16',
#				'target' => 'test',
#				'target' => 'Amoena',
#				'user_id' => '104203', // sergey@speechpad.com
#				'user_id' => '273773', // miranda.evans@amoena.com
#				'user_id' => '288009', // Andrew@tribute.co

#				'margin' => 40,
#				'margin_top' => -35, // highest
#				'margin_top' => 280, // lower
#				'margin_middle' => 120, // higher
#				'margin_middle' => -60, // lower
#				'margin_bottom' => 120,
			];
			if (false) {
			} elseif ($stylePreset === 'black_bar' && in_array($job->userId(), [
				322286, // jstepanek@jenningsco.com
			])) {
				$params = array_merge($params, [
					'overlay_width' => 1100,
				]);
			} elseif ($stylePreset === 'black_bar' && in_array($job->userId(), [
					360596, // office@aretechiro.com
			])) {
				$params = array_merge($params, [
					'overlay_width' => 900,
				]);
			} elseif (in_array($job->userId(), [
				152603, // purchases@mindvalley.com
			])) {
				$params = array_merge($params, [
					'style' => 'ass',
					'fontname' => 'Trebuchet MS',
					'fontsize' => '67',
					'outline' => '4',
					'margin' => '37',
				]);
			} elseif (in_array($job->userId(), [
				312003, // stesmith@coh.org
			])) {
				$params = array_merge($params, [
					'style' => 'ass',
					'fontname' => 'Futura md bt',
					'fontsize' => '60',
					'fontcolor' => '&H1a1a1a',
					'margin' => '0',
					'margin_middle' => '200',
					'middle_to_bottom' => '1',
					'margin_h' => '100',
				]);
			} elseif (in_array($job->userId(), [
				365286, // matthew.rossino@choa.org
			])) {
				$params = array_merge($params, [
					'style' => 'ass',
					'fontname' => 'Gotham Rounded',
					'shadow' => '2',
				]);
			}
			$this->sp_event_manager->publish('job.burn_subtitles', array_merge($params, $this->subtitleOptions));
			if ($i < $max_i) {
				sleep(10);
			}
		}
	}

	/**
	 * @param string $since
	 * @param string $to
	 * @param bool $publish
	 * @throws Exception
	 */
	public function invoice_revenue($since = '-1 month', $to = 'tomorrow', $publish = false) {
		$since_date = new DateTime($since);
		$to_date = new DateTime($to);

		$this->load->model('Invoice_model');
		$this->load->model('Customer_activity_daily_model');
		$this->load->model('Order_model');
		$this->load->helper('format');
		/** @var Invoice[] $invoices */
		$invoices = $this->Invoice_model->find([
			"created > '{$since_date->format('Y-m-d')}'",
			"created < '{$to_date->format('Y-m-d')}'",
			'paid is not null',
			'deleted is null',
		], null, 'id DESC');
		apr('ID', 6, 'white');
		apr('USER', 8, 'white');
		apr('AP', 3, 'white');
		apr('CREATED', 12, 'white');
		apr('PAID', 12, 'white');
		apr('MPAID', 12, 'white');
		apr('AMOUNT', 10, 'white');
		apr('    ACTIVITY', 20, 'white');
		apr('MARKED', 10, 'white');
		aprl('PMT METHOD', 0, 'white');

		$act = function ($user_id, $date) {
			$paid_date = new DateTime($date);
			$next_date = new DateTime($date);
			$next_date = $next_date->modify('+1 day');

			/** @var Customer_activity_daily[] $activity */
			$activity = $this->Customer_activity_daily_model->find([
				'customer_id' => $user_id,
				'date' => $paid_date->format('Y-m-d'),
			]);
			$activity_amount = !empty($activity) ? $activity[0]->paymentsTotal() : 0;

			/** @var Order[] $orders */
			$orders = $this->Order_model->find([
				'user_id' => $user_id,
				'accounting_object_type' => 'Payment',
				"created > '{$paid_date->format('Y-m-d')}'",
				"created < '{$next_date->format('Y-m-d')}'",
			]);
			$orders_amount = 0;
			foreach ($orders as $order) {
				$orders_amount += $order->getAggregateAmount();
			}
			return $activity_amount - $orders_amount;
		};
		$get_activity = $act->bindTo($this);

		$total = 0;
		foreach ($invoices as $invoice) {
			$user = $invoice->getCustomerUser();

			$activity_amount = $get_activity($user->id(), $invoice->paid());
			if (empty($activity_amount)) {
				if ($invoice->markedPaidUserId()) {
					$color = 'bold_red';
				} else {
					$color = 'red';
				}
			} elseif ($invoice->invoiceAmount() > $activity_amount) {
				$color = 'yellow';
			} elseif ($invoice->invoiceAmount() < $activity_amount) {
				$color = 'blue';
			} elseif ($invoice->userAutoPay() || $invoice->markedPaidUserId()) {
				$color = 'bold_green';
			} else {
				$color = 'green';
			}

			if ($color !== 'red') continue;

			$paid_before = substr($invoice->paid(), 0, 10) < substr($invoice->markedPaid(), 0, 10);
			$activity2 = '';
			$color2 = '';
			if ($paid_before && $color !== 'green' && $color !== 'bold_green') {
				$activity2 = $get_activity($user->id(), $invoice->markedPaid());
				if (empty($activity2)) {
					if ($invoice->markedPaidUserId()) {
						$color2 = 'bold_red';
					} else {
						$color2 = 'red';
					}
				} elseif ($invoice->invoiceAmount() > $activity2) {
					$color2 = 'yellow';
				} elseif ($invoice->invoiceAmount() < $activity2) {
					$color2 = 'blue';
				} elseif ($invoice->userAutoPay() || $invoice->markedPaidUserId()) {
					$color2 = 'bold_green';
				} else {
					$color2 = 'green';
				}
				$activity2 = format_money($activity2);
			}

			apr($invoice->id(), 6, 'bold_cyan');
			apr($user->id(), 8);
			apr($invoice->userAutoPay() ? 'Y' : 'N', 3);
			apr($invoice->created(), 12);
			apr($invoice->paid(), 12, $invoice->invoiced() ? 'white' : '');
			apr($invoice->markedPaid(), 12, $paid_before ? '' : 'dark_gray');
			apr(format_money($invoice->invoiceAmount()), 10, 'bold_blue');
			apr(format_money($activity_amount), 10, $color);
			apr($activity2, 10, $color2);
			apr($invoice->markedPaidUserId() ?: '', 10);
			aprl($invoice->paymentMethod());

			if ($publish) {
				$invoice->publishPaymentEvent();
			}

			$total++;
		}
		echo 'Total: ' . $total . '/' . count($invoices);
	}

	public function invoice_publish($invoice_id) {
		$this->load->model('Invoice_model');
		/** @var Invoice $invoice */
		$invoice = $this->Invoice_model->findById($invoice_id);
		if (empty($invoice)) return;

		$invoice->publishPaymentEvent();
	}

	public function generate_cache() {
		$uri = 'report://job_report#filters=(ordered=7+days);instance=s1;cache_id=re-generate';
		$report = $this->sp_reporting->initReportFromUri($uri);
		$c = $report->generateCache();
		echo 'Cache: ';
		aprl($c->id(), 0, 'bold_cyan');
		$url = config_item('base_url') . $report->getUrl() . '#filters=(ordered=7+days);instance=s1';
		echo "Cache: {$url}\n";
	}

	public function wipe_cache($cache_name = 'new_workers') {
		$this->load->model('reporting/Reports_cache_model');
		$conditions = [];
		if (!empty($cache_name) && $cache_name != 'all') {
			$caches = explode(',', $cache_name);
			$conditions['name'] = $caches;
		}
		/** @var Reports_cache_entity[] $all */
		$all = $this->Reports_cache_model->find($conditions);
		foreach ($all as $cache) {
			$cache->deleteData();
			$this->Reports_cache_model->delete($cache->id());
		}
	}

	public function run_cache_scheduled_tasks() {
		$this->load->model('reporting/Reports_cache_task_model');
		do {
			$tasks = $this->Reports_cache_task_model->get_scheduled_tasks(1);
			foreach ($tasks as $task) {
				$task->call();
			}
		} while (!empty($tasks));
	}

	/////////////////////
	// UTILITY METHODS
	//

	protected $time = [];

	protected function time($label, $echo = true) {
		if (empty($this->time[ $label ])) {
			$this->time[ $label ] = microtime(true);
			return null;
		}
		$elapsed = round(microtime(true) - $this->time[ $label ], 4);
		unset($this->time[ $label ]);
		if ($echo) {
			echo("{$label}: {$elapsed}\n");
		}
		return $elapsed;
	}

	public function pr($string, $length = 0, $color = '') {
		if ($string instanceof \Exception) {
			$this->pr(get_class($string), 0, 'yellow');
			if ($string->getMessage()) {
				echo ': ';
				$this->pr($string->getMessage(), 0, 'red');
			}
			return;
		} elseif ($string instanceof \DateTime) {
			$string = $string->format('Y-m-d H:i:s');
			if ($length < 0) {
				$length = 25;
			}
		} elseif (is_null($string)) {
			$string = '(NULL)';
		}
		if ($length > 0) {
			if (mb_strlen($string) > ($length - 1)) {
				$string = mb_substr($string, 0, $length - 2) . '… ';
			}
			$pad_length = $length - mb_strlen($string);
			if ($pad_length > 0) {
				$string .= str_repeat(' ', $pad_length);
			}
		}
		if (!empty($color)) {
			$string = Color::fg($color, $string);
		}
		echo $string;
	}

	public function prl($string, $length = 0, $color = '') {
		$this->pr($string, $length, $color);
		echo "\n";
	}

	public function wait($seconds) {
		echo "\n";
		for ($i = 0; $i < $seconds; $i++) {
			echo "\r$i";
			sleep(1);
		}
		echo "\r\033[K";
	}

	protected function read_line($invisible = false) {
		$old_state = trim(shell_exec('stty --save'));
		if ($invisible) shell_exec('stty -echo');
		$line = fgets(STDIN);
		shell_exec("stty '{$old_state}'");
		return rtrim($line, "\r\n");
	}

	protected function read_char($invisible = false) {
		$old_state = trim(shell_exec('stty --save'));
		if ($invisible) shell_exec('stty -echo');
		shell_exec('stty -icanon');
		$char = fread(STDIN, 1);
		shell_exec("stty '{$old_state}'");
		return $char;
	}

	protected function sort_text($text) {
		$lines = explode("\n", $text);
		sort($lines);
		return implode("\n", $lines);
	}

	protected $debugRest = false;
	protected function call_rest($service, $operation, $params = [], $user_id = 1, $extras = []) {
		$user = $this->User_model->findById($user_id);
		$this->load->library('REST_Client');
		$params = array_replace([
			'service_name' => $service,
			'service_version' => '1.0.0',
			'access_key' => $user->accessKey(),
			'secret_key' => $user->secretKey(),
//			'api_profiling' => ($user->isAdministrator() ? '1' : '0'),
			'format' => 'serialize',
			'method' => 'get',
			'operation' => $operation,
		], $params);
		if (!empty($this->debugRest)) {
			$params['XDEBUG_SESSION_START'] = 'true';
		}
		$response = $this->rest_client->call($params, $extras);
		return @unserialize($response);
	}

	protected function render_email_template($template, $params = []) {
		$this->load->library('messenger/email_template');
		/** @var \Email_template $template */
		$template = $this->email_template->getTemplate($template, $params);
		return $template->getHTML();
	}

	protected $key = 'my_sup3r_seCr3t_Key';

	public function encrypt($value) {
		echo $this->_encrypt($value);
	}

	protected function _encrypt($value) {
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$e = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->key, $value, MCRYPT_MODE_CBC, $iv);
		return base64_encode($iv . $e);
	}

	public function decrypt($value) {
		echo $this->_decrypt($value);
	}

	protected function _decrypt($value) {
		$e = base64_decode($value);
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv_dec = substr($e, 0, $iv_size);
		$ciphertext_dec = substr($e, $iv_size);
		$d = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);
		return rtrim($d, "\0");
	}

	public function login_url($user_id) {
		if (!defined('CMD')) {
			return;
		}
		/** @var \User $user */
		if (is_numeric($user_id)) {
			$user = $this->User_model->findById($user_id);
		} else {
			$user = $this->User_model->findByIdentity($user_id);
			if (empty($user)) {
				$user = $this->User_model->mturk_findByWorkerId($user_id);
			}
		}
		if (empty($user)) {
			return;
		}
		$this->load->model('Saved_session_model');
		/** @var \SavedSession $session */
		$session = $this->Saved_session_model->findByKey('user_id', $user->id());
		if (empty($session)) {
			$session = $this->Saved_session_model->generate([
				'session_id' => uuid(),
				'user_id' => $user->id(),
			], true);
		}
		echo config_item('base_url') . 'customer/loginasuser/' . $session->uuid();
	}
}

function apr($string, $length = 0, $color = '') {
	get_instance()->pr($string, $length, $color);
}

function aprl($string, $length = 0, $color = '') {
	apr($string, $length, $color);
	echo "\n";
}
