#
THIS_IS_IMPORTANT=1
source $HOME/.config/user-cfg/bash

export PATH=/app_cache/bin/ffmpeg:$PATH

alias vim='vim -n -p -b'
alias svn='svn --username sergey'
alias www='sudo su www-data --preserve-env'
alias wwwrun='sudo -EHu www-data '
alias sudo='sudo -E '

if [ `whoami` = "www-data" ]; then
	PS1=${_PROMPT_REMOTE_ROOT_WARN}
	if [ -d /var/www/speechpad ]; then
		cd /var/www/speechpad
	else
		cd /var/www
	fi
fi
