export GOPATH=$HOME/dev/.lib/go

if [ -z "$SSH_CLIENT" ]; then
	# start SSH agent for local sessions
	if [ -z "$SSH_AGENT_PID" ]; then
		keychain --quiet .ssh/id_rsa
		source .keychain/e440-sh
	fi
fi

if [ $TERM = "linux" ]; then
	sysstats
fi
