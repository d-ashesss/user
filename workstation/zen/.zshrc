## Edifier autoconnect
#bluetoothctl -- connect '04:FE:A1:CF:FC:70'
## Louder
# bluetoothctl -- connect 'F8:DF:15:A4:BC:EB'

if [ -z "$SSH_CLIENT" ]; then
	# start SSH agent for local sessions
	if [ -z "$SSH_AGENT_PID" ]; then
		keychain --quiet .ssh/id_rsa
		source .keychain/zen-sh
	fi
fi

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/dash/.local/share/google-cloud-sdk/path.zsh.inc' ]; then . '/home/dash/.local/share/google-cloud-sdk/path.zsh.inc'; fi
# The next line enables shell command completion for gcloud.
if [ -f '/home/dash/.local/share/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/dash/.local/share/google-cloud-sdk/completion.zsh.inc'; fi

if [[ ! "$DISPLAY" && "$XDG_VTNR" -eq 1 ]]; then
	startx
fi
